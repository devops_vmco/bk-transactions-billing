package co.com.virgin.transactions.billing.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageResult;

import co.com.virgin.commons.util.response.ServiceException;

/**
 * Servicio de transacciones
 */
@Service(value = "messageProductorService")
public class MessageProductorServiceImpl implements MessageProductorService {

	/**
	 * Logger instance
	 */
	private static final Logger LOG = LogManager.getLogger("bkCentralizedTransactionsLogger");

	@Value("${aws.access.key}")
	private String awsAccessKey;
	
	@Value("${aws.secret.key}")
	private String awsSecretKey;

	@Override
	public Boolean sendMessageToQueue(String codeScheduling, String queueUrl) throws ServiceException {
		Boolean flag = false;
		try {
			BasicAWSCredentials awsCredentials = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
			AmazonSQS sqs = AmazonSQSClientBuilder.standard().withRegion(Regions.US_EAST_1)
					.withCredentials(new AWSStaticCredentialsProvider(awsCredentials)).build();
			SendMessageResult sendMessageResult = sendMessage(codeScheduling, sqs, queueUrl);
			if (sendMessageResult != null && sendMessageResult.getMessageId() != null) {
				LOG.info("   >>>>>>>>>>>>>> MENSAJE: " + codeScheduling + "  ENCOLADO CORRECTAMENTE EN:  " + queueUrl);
				flag = true;
			} else {
				LOG.info("   >>>>>>>>>>>>>> MENSAJE: " + codeScheduling + "  FALLÓ AL ENCOLAR EN:  " + queueUrl);
				flag = false;
			}
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	public SendMessageResult sendMessage(String transactionId, AmazonSQS sqs, String urlQueue) {

		SendMessageResult sendMessageResult = new SendMessageResult();
		try {

			final SendMessageRequest sendMessageRequest = new SendMessageRequest(urlQueue, transactionId);
			/*
			 * When you send messages to a FIFO queue, you must provide a non-empty
			 * MessageGroupId.
			 */
			sendMessageRequest.setMessageGroupId(transactionId);

			// Uncomment the following to provide the MessageDeduplicationId
			// sendMessageRequest.setMessageDeduplicationId("1");
			sendMessageResult = sqs.sendMessage(sendMessageRequest);
			final String sequenceNumber = sendMessageResult.getSequenceNumber();
			final String messageId = sendMessageResult.getMessageId();
			System.out.println(
					"SendMessage succeed with messageId " + messageId + ", sequence number " + sequenceNumber + "\n");
		} catch (final AmazonServiceException ase) {
			sendMessageResult = null;
			LOG.error("Caught an AmazonServiceException, which means " + "your request made it to Amazon SQS, but was "
					+ "rejected with an error response for some reason.");
			LOG.error("Error Message:    " + ase.getMessage());
			LOG.error("HTTP Status Code: " + ase.getStatusCode());
			LOG.error("AWS Error Code:   " + ase.getErrorCode());
			LOG.error("Error Type:       " + ase.getErrorType());
			LOG.error("Request ID:       " + ase.getRequestId());
		} catch (final AmazonClientException ace) {
			sendMessageResult = null;
			LOG.error("Caught an AmazonClientException, which means "
					+ "the client encountered a serious internal problem while "
					+ "trying to communicate with Amazon SQS, such as not " + "being able to access the network.");
			LOG.error("Error Message: " + ace.getMessage());
		} catch (Exception e) {
			LOG.error(
					"Ocurrió un error indeterminado al Encolar el Mensaje: " + transactionId + "   " + e.getMessage());
		}

		return sendMessageResult;

	}

}
