package co.com.virgin.transactions.billing.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
* La clase de persistencia para la base de datos 
* @author crist
*
*/
@Entity
@Table(name = "inew_rtm_sales")
@NamedQuery(name = "TransactionBillingRTM.findAll", query = "SELECT bt FROM TransactionBillingRTM bt")
public class TransactionBillingRTM implements Serializable{
	private static final long serialVersionUID = 1L;
	
	
	
	@Column(name = "rtm_id")
	private String rtmId;
	
	@Column(name = "retailer")
	private String retailer;
	
	@Column(name = "nit")
	private String nit;
	
	
	@Column(name = "transaction")
	private Long transaction;
	
	@Column(name = "type")
	private BigInteger type;
	
	@Id
	@Column(name = "approval")
	private String approval;
	
	@Column(name = "sale_date")
	private Date sale_date;
	
	@Column(name = "amount")
	private BigInteger amount;
	
	@Column(name = "percentage_discount")
	private BigInteger percentageDiscount;
	
	@Column(name = "amount_discount")
	private BigInteger amountDiscount;
	
	@Column(name = "tax")
	private BigInteger tax;
	
	@Column(name = "add_balance")
	private BigInteger addBalance;
	
	@Column(name = "cut")
	private String cut;
	
	@Column(name = "prefix")
	private BigInteger prefix;
	
	@Column(name = "tipo")
	private BigInteger tipo;

	@Column(name = "status")
	private String status;
	
	@Column(name = "transactions_attempts")
	private int transactionsAttempts;
	
	
	
	public int getTransactionsAttempts() {
		return transactionsAttempts;
	}
	

	public void setTransactionsAttempts(int transactionsAttempts) {
		this.transactionsAttempts = transactionsAttempts;
	}

	
	public TransactionBillingRTM() {
		
	}
	
	public String getRtmId() {
		return rtmId;
	}

	public void setRtmId(String rtmId) {
		this.rtmId = rtmId;
	}

	public String getRetailer() {
		return retailer;
	}

	public void setRetailer(String retailer) {
		this.retailer = retailer;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public Long getTransaction() {
		return transaction;
	}

	public void setTransaction(Long transaction) {
		this.transaction = transaction;
	}

	public BigInteger getType() {
		return type;
	}

	public void setType(BigInteger type) {
		this.type = type;
	}

	public String getApproval() {
		return approval;
	}

	public void setApproval(String approval) {
		this.approval = approval;
	}

	public Date getSale_date() {
		return sale_date;
	}

	public void setSale_date(Date sale_date) {
		this.sale_date = sale_date;
	}

	public BigInteger getAmount() {
		return amount;
	}

	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}

	public BigInteger getPercentageDiscount() {
		return percentageDiscount;
	}

	public void setPercentageDiscount(BigInteger percentageDiscount) {
		this.percentageDiscount = percentageDiscount;
	}

	public BigInteger getAmountDiscount() {
		return amountDiscount;
	}

	public void setAmountDiscount(BigInteger amountDiscount) {
		this.amountDiscount = amountDiscount;
	}

	public BigInteger getTax() {
		return tax;
	}

	public void setTax(BigInteger tax) {
		this.tax = tax;
	}

	public BigInteger getAddBalance() {
		return addBalance;
	}

	public void setAddBalance(BigInteger addBalance) {
		this.addBalance = addBalance;
	}

	public String getCut() {
		return cut;
	}

	public void setCut(String cut) {
		this.cut = cut;
	}

	public BigInteger getPrefix() {
		return prefix;
	}

	public void setPrefix(BigInteger prefix) {
		this.prefix = prefix;
	}

	public BigInteger getTipo() {
		return tipo;
	}

	public void setTipo(BigInteger tipo) {
		this.tipo = tipo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return new com.google.gson.Gson().toJson(this);
	}
	
}
