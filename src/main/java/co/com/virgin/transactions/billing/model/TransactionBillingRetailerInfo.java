package co.com.virgin.transactions.billing.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;
/*
 * 
 * 
 * */
@Entity
@Table(name = "retailers_info")
@NamedQuery(name = "TransactionBillingRetailerInfo.findAll", query = "SELECT bt FROM TransactionBillingRetailerInfo bt")
public class TransactionBillingRetailerInfo implements Serializable {
	
	
	private static final long serialVersionUID = 1L;
	
	public TransactionBillingRetailerInfo()
	{
		
	}
	
	@Id
	@Column(name = "nit")
	private String nit;
	
	@Column(name = "retailer_name")
	private String retailerName;
	
	@Column(name = "email")
	private String email;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "code_city")
	private String codeCity;
	
	@Column(name = "id_departamento")
	private String idDepartamento;

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getRetailerName() {
		return retailerName;
	}

	public void setRetailerName(String retailerName) {
		this.retailerName = retailerName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCodeCity() {
		return codeCity;
	}

	public void setCodeCity(String codeCity) {
		this.codeCity = codeCity;
	}

	public String getIdDepartamento() {
		return idDepartamento;
	}

	public void setIdDepartamento(String idDepartamento) {
		this.idDepartamento = idDepartamento;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
