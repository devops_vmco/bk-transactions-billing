package co.com.virgin.transactions.billing.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import co.com.virgin.transactions.billing.model.TransactionBillingRTMProcessed;

public interface TransactionBillingRTMProcessedRepository extends JpaRepository<TransactionBillingRTMProcessed,String>{
	

}
