package co.com.virgin.transactions.billing.service;


import java.util.List;

import co.com.virgin.commons.util.response.ServiceException;
import co.com.virgin.inew.services.customer.dto.CustomerResponseDTO;
import co.com.virgin.transactions.billing.model.TransactionBillingMovilbox;

public interface TransactionBillingService {	
	
	public List<TransactionBillingMovilbox> getInfoBillingTran(String status);
	
	public void initTransactionMovilbox();
	
	public String processSalesMovilbox(Long id) throws ServiceException;
	
	public void initTransactionRTM();
	
	public String processSalesRTM(String approval) throws ServiceException;
	
	public void initTransactionRecharge();
	
	public String processSalesRecharge(Long id) throws ServiceException;
	
	

	
}