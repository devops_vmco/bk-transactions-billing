package co.com.virgin.transactions.billing.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import co.com.virgin.commons.util.response.ServiceException;
import co.com.virgin.securityaccess.dto.AuthTokenDto;

@Component
public class SecurityAccessClient {

	@Value("${url.security-access}")
	private String urlSecurityAccess;
	@Value("${time.out.security-access}")
	private String timeOutSecurityAccess;

	@Autowired
	private GenericInvocation genericInvocation;
	private ObjectMapper mapper = new ObjectMapper();

	public String getTokenInew() throws ServiceException {
		Object obj = genericInvocation.executeApi(null, HttpMethod.GET, urlSecurityAccess, "/users/key-services",
				Integer.valueOf(timeOutSecurityAccess));
		AuthTokenDto authTokenDto = mapper.convertValue(obj, AuthTokenDto.class);
		return "Bearer " + authTokenDto.getToken();
	}

}
