package co.com.virgin.transactions.billing.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import co.com.virgin.billing.fase2.dto.ActiveTransactionDTO;
import co.com.virgin.billing.fase2.dto.InvoiceAcquirerDTO;
import co.com.virgin.billing.fase2.dto.InvoiceAddFieldDTO;
import co.com.virgin.billing.fase2.dto.InvoiceDetailDTO;
import co.com.virgin.billing.fase2.dto.InvoiceDiscountDTO;
import co.com.virgin.billing.fase2.dto.InvoiceDocumentHeaderDTO;
import co.com.virgin.billing.fase2.dto.InvoicePaymentDTO;
import co.com.virgin.billing.fase2.dto.InvoicePaymentMehotdDTO;
import co.com.virgin.billing.fase2.dto.InvoiceTaxDTO;
import co.com.virgin.commons.util.generic.Constants;
import co.com.virgin.commons.util.generic.GeneralMessages;
import co.com.virgin.commons.util.generic.StringUtils;
import co.com.virgin.commons.util.response.ApiError;
import co.com.virgin.commons.util.response.ServiceException;
import co.com.virgin.inew.services.customer.dto.CustomerResponseDTO;
import co.com.virgin.inew.services.customer.dto.SubscriptionCriteria;
import co.com.virgin.mipaquete.dto.Town;
import co.com.virgin.transactions.billing.client.GenericInvocation;
import co.com.virgin.transactions.billing.client.MessageProductorService;
import co.com.virgin.transactions.billing.client.SecurityAccessClient;
import co.com.virgin.transactions.billing.model.TransactionBillingMovilbox;
import co.com.virgin.transactions.billing.model.TransactionBillingRTM;
import co.com.virgin.transactions.billing.model.TransactionBillingRTMProcessed;
import co.com.virgin.transactions.billing.model.TransactionBillingRecharge;
import co.com.virgin.transactions.billing.model.TransactionBillingRetailerInfo;
import co.com.virgin.transactions.billing.repository.TransactionBillingMovilboxRepository;
import co.com.virgin.transactions.billing.repository.TransactionBillingRTMProcessedRepository;
import co.com.virgin.transactions.billing.repository.TransactionBillingRTMRepository;
import co.com.virgin.transactions.billing.repository.TransactionBillingRechargeRepository;
import co.com.virgin.transactions.billing.repository.TransactionBillingRetailerInfoRepository;
import co.com.virgin.transactions.dto.TransactionStatusEnum;

@Service("TransactionBillingService")
public class TransactionBillingServiceImpl implements TransactionBillingService {

	// TODO: Generar repository
	@Autowired
	TransactionBillingMovilboxRepository repositoryMovilBox;

	@Autowired
	TransactionBillingRTMRepository repositoryRTM;

	@Autowired
	TransactionBillingRTMProcessedRepository repositoryRTMProcessed;

	@Autowired
	TransactionBillingRechargeRepository repositoryRecharge;

	@Autowired
	private MessageProductorService messageProductorService;

	@Autowired
	private TransactionBillingRetailerInfoRepository retailersRepository;

	@Autowired
	private SecurityAccessClient inewSecurityAccess;

	@Value("${sqs.transactions.billing.movilbox.url}")
	private String sqsTransactionsBillingMovilboxUrl;

	@Value("${sqs.transactions.billing.movilbox.url.failed}")
	private String sqsTransactionsBillingMovilboxUrlFailed;

	@Value("${sqs.transactions.billing.rtm.url}")
	private String sqsTransactionsBillingRTMUrl;

	@Value("${sqs.transactions.billing.rtm.url.failed}")
	private String sqsTransactionsBillingRTMUrlFailed;

	@Value("${sqs.transactions.billing.recharge.url}")
	private String sqsTransactionsBillingRechargeUrl;

	@Value("${sqs.transactions.billing.recharge.url.failed}")
	private String sqsTransactionsBillingRechargeUrlFailed;

	@Value("${endpoint.billing}")
	private String urlBillinV2;

	@Value("${billing.timeout}")
	private int billingTimeout;

	@Value("${endpoint.mipaquete}")
	private String endpointMipaquete;

	@Value("${time.out.mipaquete}")
	private Integer timeOutMipaquete;

	@Value("${channels.movilbox.excluded}")
	private String channelsMovilboxExcluded;

	@Value("${points.movilbox.retail.acepted}")
	private String pointsMovilboxRetailAcepted;

	@Value("${dev.billing.email.mode}")
	private boolean devBillingEmailMode;

	@Value("${endpoint.inew}")
	private String endpointInew;

	@Value("${time.out.inew}")
	private String timeOutInew;

	private static final Logger logger = LogManager.getLogger("transactionBilling");

	public static final String DEFAULT_APLICA_FEL = "SI";
	public static final String DEFAULT_EMAIL = "facturacion.clientes@virginmobilecolombia.com";
	public static final String DEFAULT_EMAIL_TEST = "cristian.ramirez@complemento360.com";
	public static final String DEFAULT_DEPARTAMENTO = "Bogotá D.C.";
	public static final String DEFAULT_CIUDAD = "Bogotá D.C.";
	public static final String DEFAULT_BARRIO = "Chico";
	public static final String DEFAULT_MONEDA = "COP";
	public static final String DEFAULT_PAIS = "CO";
	public static final String DEFAULT_REGIMEN = "2";
	public static final String DEFAULT_TIPO_COMPRA = "1";
	public static final String DEFAULT_CANAL_1 = "WEB_APP";
	public static final String DEFAULT_CANAL_2 = "INEW-MOVILBOX";
	public static final String DEFAULT_CODIGO_RETENCION = "01";
	public static final String DEFAULT_DIRECCION = "Calle 93B # 13 - 91 Edificio Meridiano";
	public static final String DEFAULT_RAZON_SOCIAL = "Cuantías menores";
	public static final String DEFAULT_MSISDN = "3190000000";
	public static final String EMAIL = "EMAIL";
	public static final String DEFAULT_VERSION = "7";
	public static final String DEFAULT_TIPO_DOCUMENTO = "1";
	public static final String DEFAULT_DEPARTAMENTO_CODE = "11";
	public static final String DEFAULT_CIUDAD_CODE = "11001";
	public static final boolean DEFAULT_AUTO_RETENIDO = false;
	public static final String DEFAULT_MEDIO_PAGO = "49";
	public static final Integer DEFAULT_LINEAS = 1;
	public static final String DEFAULT_TIPO_OPERACION = "10";
	public static final String DEFAULT_UNIDAD_MEDIDAD = "C58";
	public static final Integer DEFAULT_TIPO_IMPUESTO = 1;
	public static final String DEFAULT_TIPO_CODIGO_PRODUCTO = "001";
	public static final Integer DEFAULT_CODIGO_PLANTILLA = 1;
	public static final String DEFAULT_CHANNEL_MOVILBOX = "MOVILBOX";
	public static final String DEFAULT_CHANNEL_RTM = "RTM";
	public static final String DEFAULT_CHANNEL_ATH = "ATH";
	public static final String DEFAULT_CHANNEL_IVR = "IVR";
	public static final String DEFAULT_TIPO_NIT = "31";
	public static final String DEFAULT_CHANNEL_DAVIVIENDA = "DAVIVIENDA";
	private static final String DEFAULT_TIPO_PRODUCTO = "TOPUP";
	private static final String DEFAULT_CODE_CEDULA = "13";
	private static final String DEFAULT_TIPO_PROD = "RECARGA";

	// DATOS DUMMY
	private static final String DEFAULT_NIT_VIRGIN = "9004201227";
	private static final String DEFAULT_TELEFONO = "7366720";

	@Autowired
	private GenericInvocation genericInvocation;

	private GeneralMessages generalMessages = new GeneralMessages();

	private ObjectMapper mapper = new ObjectMapper();

	@Override
	public List<TransactionBillingMovilbox> getInfoBillingTran(String status) {

		List<TransactionBillingMovilbox> data = repositoryMovilBox.findByStatus(status);

		return data;
	}

	@Override
	public void initTransactionMovilbox() {

		logger.info("################### init initTransactionMovilbox ##########");

		ArrayList<String> listaCanalesExcluidos = new ArrayList<String>(
				Arrays.asList(channelsMovilboxExcluded.split(",")));
		ArrayList<String> listaPuntosAceptados = new ArrayList<String>(
				Arrays.asList(pointsMovilboxRetailAcepted.split(",")));

		List<TransactionBillingMovilbox> data = repositoryMovilBox.findByStatus("CHARGED");

		for (TransactionBillingMovilbox venta : data) {
			if (!listaCanalesExcluidos.contains(venta.getTipoCanal())
					|| listaPuntosAceptados.contains(venta.getNombreCanal())) {
				try {
					if (messageProductorService.sendMessageToQueue(venta.getId() + "",
							sqsTransactionsBillingMovilboxUrl)) {
						logger.info("<" + venta.getId() + "> Venta encolada para validación " + venta.getId());
						venta.setStatus("QUEUED");
						repositoryMovilBox.save(venta);
					} else
						logger.info("<" + venta.getId() + "> Venta  NO encolada para validación " + venta.getId());
				} catch (Exception e) {
					logger.info("<" + venta.getId() + "> Ocurrio un error al encolar la Venta" + venta.getId());
					logger.info("<" + venta.getId() + "> " + e.getMessage());
				}
			} else {
				logger.info("Venta [" + venta.getIdVenta() + "] no facturada por exclusion de canal");
				venta.setStatus("NOT_BILLING");
				repositoryMovilBox.save(venta);
			}
		}
		logger.info("################### end initTransactionMovilbox ##########");

	}

	private static String validateLength(String field, int maxLength) {

		if (null == field) {
			return field;
		}

		if (field.length() > maxLength)
			return field.substring(0, maxLength - 1);
		else
			return field;
	}

	private InvoiceDocumentHeaderDTO createBillRequestMovilBox(TransactionBillingMovilbox facturaMovilbox)
			throws ServiceException {
		InvoiceDocumentHeaderDTO request = new InvoiceDocumentHeaderDTO();
		Town datosCiudad = getCity(facturaMovilbox.getCiudad());

		request.setVersion(DEFAULT_VERSION);

		request.setTipodocumento(DEFAULT_TIPO_DOCUMENTO);

		request.setCantidadLineas(DEFAULT_LINEAS);

		Timestamp tsFechaVenta = new Timestamp(facturaMovilbox.getFechaVenta().getTime());
		request.setFechafacturacion(tsFechaVenta);

		request.setTipoOperacion(DEFAULT_TIPO_OPERACION);

		request.setCodigoPlantillaPdf(DEFAULT_CODIGO_PLANTILLA);

		// Datos de transaccion
		ActiveTransactionDTO activeTransaction = new ActiveTransactionDTO();
		activeTransaction.setChannel(DEFAULT_CHANNEL_MOVILBOX);
		activeTransaction.setChannel_type(DEFAULT_CHANNEL_MOVILBOX);
		activeTransaction.setId_transaction(facturaMovilbox.getIdVenta());

		if (null != facturaMovilbox.getReferencia()) {
			activeTransaction.setProduct_code(facturaMovilbox.getSerial());
			activeTransaction.setProduct_name(facturaMovilbox.getReferencia());
		}
		activeTransaction.setProduct_value(facturaMovilbox.getValor());
		activeTransaction.setMsisdn(DEFAULT_MSISDN);

		request.setActiveTransaction(activeTransaction);

		// Datos adquierente
		InvoiceAcquirerDTO adquiriente = new InvoiceAcquirerDTO();

		if (!"N/A".equals(facturaMovilbox.getNroDocumento()) && validateNumber(facturaMovilbox.getNroDocumento())
				&& !"0".equals(facturaMovilbox.getNroDocumento())) {
			adquiriente.setNumeroIdentificacion(facturaMovilbox.getNroDocumento());
		} else {
			adquiriente.setNumeroIdentificacion(DEFAULT_NIT_VIRGIN);
		}

		adquiriente.setCiudad(datosCiudad.getDaneTown());

		adquiriente.setDepartamento(
				datosCiudad.getDaneDept().length() == 1 ? "0" + datosCiudad.getDaneDept() : datosCiudad.getDaneDept());

		// Se valida para que no viajen mas de 100 caracteres
		adquiriente.setDirección(validateLength(facturaMovilbox.getDireccion(), 100));

		// Se valida para que no viajen mas de 50 caracteres
		if (null != facturaMovilbox.getDireccion())
			adquiriente.setBarioLocalidad(validateLength(facturaMovilbox.getBarrio(), 50));

		if (!"N/A".equals(facturaMovilbox.getCorreoElectronico())) {
			adquiriente.setEmail(facturaMovilbox.getCorreoElectronico());
		} else {
			adquiriente.setEmail(DEFAULT_EMAIL);
		}

		if (!"N/A".equals(facturaMovilbox.getTipoDocumento()) && "1".equals(facturaMovilbox.getTipoDocumento())) {
			adquiriente.setTipoIdentificacion(Integer.parseInt(facturaMovilbox.getTipoDocumento()));
		} else {
			adquiriente.setTipoIdentificacion(Integer.parseInt(DEFAULT_TIPO_NIT));
		}

		adquiriente.setPais(DEFAULT_PAIS);
		adquiriente.setTelefono(DEFAULT_TELEFONO);

		// TODO Se toma el tipo persona como el tipo de identificacion dentro de la base
		// de datos por tema de aclaracion del nombre del campo en base de datos
		if (null != facturaMovilbox.getTipoIdentificacion() && !"0".equals(facturaMovilbox.getTipoIdentificacion())
				&& !"N/A".equals(facturaMovilbox.getTipoIdentificacion())
				&& !"1".equals(facturaMovilbox.getTipoIdentificacion())) {
			adquiriente.setTipoIdentificacion(Integer.parseInt(facturaMovilbox.getTipoIdentificacion()));
		} else {
			adquiriente.setTipoIdentificacion(Integer.parseInt(DEFAULT_CODE_CEDULA));
		}
		
		if (null == facturaMovilbox.getNombres() || "N/A".equals(facturaMovilbox.getNombres())) {
			adquiriente.setTipoPersona(DEFAULT_TIPO_DOCUMENTO);
		} else {
			adquiriente.setTipoPersona(DEFAULT_REGIMEN);
		}
		
		
		adquiriente.setNombreCompleto(facturaMovilbox.getNombres() + " " + facturaMovilbox.getApellidos());
		adquiriente.setEnvioPorEmailPlataforma(EMAIL);
		List<InvoiceAcquirerDTO> listaAdquirentes = new ArrayList<InvoiceAcquirerDTO>();
		listaAdquirentes.add(adquiriente);
		request.setListaAdquirentes(listaAdquirentes);

		// Descuentos
		InvoiceDiscountDTO descuento = new InvoiceDiscountDTO();
		descuento.setDescuento(new BigDecimal(0.0));
		descuento.setPorcentajeDescuento(new BigDecimal(0.0));
		List<InvoiceDiscountDTO> listaDescuentos = new ArrayList<InvoiceDiscountDTO>();
		listaDescuentos.add(descuento);
		request.setListaDescuentos(listaDescuentos);

		request.setAplicafel(DEFAULT_APLICA_FEL);
		// Datos Pago de la factura
		InvoicePaymentDTO invoicePayment = new InvoicePaymentDTO();
		invoicePayment.setMoneda(DEFAULT_MONEDA);
		invoicePayment.setTipocompra(new Integer(DEFAULT_TIPO_COMPRA));
		invoicePayment.setFechavencimiento(facturaMovilbox.getFechaVenta());
		invoicePayment.setCodigoMonedaCambio(DEFAULT_MONEDA);

		invoicePayment.setTotalfactura(new BigDecimal(facturaMovilbox.getValor()));
		invoicePayment.setTotalimportebruto(new BigDecimal(facturaMovilbox.getValor()));
		request.setPago(invoicePayment);

		// Armamos la lista detalle de la factura
		List<InvoiceDetailDTO> listaDetalle = new ArrayList<InvoiceDetailDTO>();
		if (null != request.getListaDetalle())
			listaDetalle = request.getListaDetalle();
		InvoiceDetailDTO invoiceDetail = new InvoiceDetailDTO();
		invoiceDetail.setCantidad(new BigDecimal(1));
		invoiceDetail.setDescripcion(facturaMovilbox.getReferencia());
		// invoiceDetail.setCodigoproducto(detalleFactura.getTransactionId());
		invoiceDetail.setValorunitario(new BigDecimal(facturaMovilbox.getValor()));
		invoiceDetail.setPreciototal(new BigDecimal(facturaMovilbox.getValor()));
		invoiceDetail.setPreciosinimpuestos(new BigDecimal(facturaMovilbox.getValor()));
		invoiceDetail.setReferencia(facturaMovilbox.getReferencia());
		invoiceDetail.setTipocodigoproducto(DEFAULT_TIPO_CODIGO_PRODUCTO);

		// Se valida en caso de que sea recarga

		if (!facturaMovilbox.getTipoProducto().equals("RECARGA"))
			invoiceDetail.setCodigoproducto(facturaMovilbox.getSerial());
		else
			invoiceDetail.setCodigoproducto(DEFAULT_TIPO_PRODUCTO);

		invoiceDetail.setNombreProducto(facturaMovilbox.getReferencia());
		invoiceDetail.setUnidadmedida(DEFAULT_UNIDAD_MEDIDAD);

		invoiceDetail.setTipoImpuesto(DEFAULT_TIPO_IMPUESTO);

		InvoiceDiscountDTO descuentoAux = new InvoiceDiscountDTO();
		descuento.setDescuento(new BigDecimal(0.0));
		descuento.setPorcentajeDescuento(new BigDecimal(0.0));
		List<InvoiceDiscountDTO> listaDescuentosAux = new ArrayList<InvoiceDiscountDTO>();
		invoiceDetail.setListaDescuento(listaDescuentosAux);

		InvoiceTaxDTO bfelImpuestos = new InvoiceTaxDTO();
		bfelImpuestos.setBaseimponible(new BigDecimal(facturaMovilbox.getValor()));
		bfelImpuestos.setCodigoImpuestoRetencion("01");
		bfelImpuestos.setPorcentaje(new BigDecimal(0));
		bfelImpuestos.setValorImpuestoRetencion(new BigDecimal(0));
		bfelImpuestos.setIsAutoRetenido(DEFAULT_AUTO_RETENIDO);
		List<InvoiceTaxDTO> impuestosItem = new ArrayList<InvoiceTaxDTO>();
		impuestosItem.add(bfelImpuestos);
		invoiceDetail.setListaImpuestos(impuestosItem);

		listaDetalle.add(invoiceDetail);

		// Armamos la lista de campos adicionales
		List<InvoiceAddFieldDTO> listaCampos = new ArrayList<InvoiceAddFieldDTO>();
		InvoiceAddFieldDTO campo = new InvoiceAddFieldDTO();
		// Datos productos

		campo.setNombreCampo("observaciones");
		if (!facturaMovilbox.getReferencia().isEmpty()) {
			campo.setValorCampo(facturaMovilbox.getReferencia());
			listaCampos.add(campo);
		}
		// Datos Cliente
		campo = new InvoiceAddFieldDTO();
		campo.setNombreCampo("codigocliente");
		campo.setValorCampo(DEFAULT_TELEFONO);
		listaCampos.add(campo);
		request.setListaCamposAdicionales(listaCampos);

		// Medios de Pago
		List<InvoicePaymentMehotdDTO> listaMediosPagos = new ArrayList<InvoicePaymentMehotdDTO>();
		InvoicePaymentMehotdDTO medioPago = new InvoicePaymentMehotdDTO();
		medioPago.setMedioPago(DEFAULT_MEDIO_PAGO);
		listaMediosPagos.add(medioPago);
		request.setListaMediosPagos(listaMediosPagos);

		request.setListaDetalle(listaDetalle);

		return request;
	}

	private InvoiceDocumentHeaderDTO createBillRequestRTM(TransactionBillingRTMProcessed facturaRTM) {
		InvoiceDocumentHeaderDTO request = new InvoiceDocumentHeaderDTO();

		TransactionBillingRetailerInfo infoRetailer = retailersRepository.findOne(facturaRTM.getNit());

		request.setVersion(DEFAULT_VERSION);
		request.setTipodocumento(DEFAULT_TIPO_DOCUMENTO);

		request.setCantidadLineas(DEFAULT_LINEAS);

		Timestamp tsFechaVenta = new Timestamp(facturaRTM.getSale_date().getTime());
		request.setFechafacturacion(tsFechaVenta);

		request.setTipoOperacion(DEFAULT_TIPO_OPERACION);

		request.setCodigoPlantillaPdf(DEFAULT_CODIGO_PLANTILLA);

		// Datos de transaccion
		ActiveTransactionDTO activeTransaction = new ActiveTransactionDTO();
		activeTransaction.setChannel(DEFAULT_CHANNEL_RTM);
		activeTransaction.setChannel_type(DEFAULT_CHANNEL_RTM);
		activeTransaction.setId_transaction("RT"+facturaRTM.getTransaction());

		
		activeTransaction.setProduct_code(DEFAULT_TIPO_PRODUCTO);
		activeTransaction.setProduct_name(DEFAULT_TIPO_PRODUCTO);
		
		activeTransaction.setProduct_value(facturaRTM.getAddBalance() + "");
		activeTransaction.setMsisdn(DEFAULT_MSISDN);

		request.setActiveTransaction(activeTransaction);

		// Datos adquierente
		InvoiceAcquirerDTO adquiriente = new InvoiceAcquirerDTO();

		if (null != infoRetailer && null != infoRetailer.getCity()) {
			adquiriente.setCiudad(infoRetailer.getCodeCity());
		} else {
			adquiriente.setCiudad(DEFAULT_CIUDAD_CODE);
		}

		if (null != infoRetailer && null != infoRetailer.getIdDepartamento()) {
			adquiriente.setDepartamento(infoRetailer.getIdDepartamento());
		} else {
			adquiriente.setDepartamento(DEFAULT_DEPARTAMENTO_CODE);
		}

		adquiriente.setNombreDepartamento(DEFAULT_DEPARTAMENTO);

		if(null != infoRetailer && null != infoRetailer.getAddress()) {			
			// Se valida para que no viajen mas de 100 caracteres
			adquiriente.setDirección(validateLength(infoRetailer.getAddress(), 100));
		}else {
			adquiriente.setDirección(DEFAULT_DIRECCION);
		}

		// Se valida para que no viajen mas de 50 caracteres
		adquiriente.setBarioLocalidad(validateLength(DEFAULT_BARRIO, 50));

		// modo de desarrollo
		if (!devBillingEmailMode) {
			if (null != infoRetailer && null != infoRetailer.getEmail()) {
				adquiriente.setEmail(infoRetailer.getEmail());
			} else {
				adquiriente.setEmail(DEFAULT_EMAIL);
			}
		} else {
			adquiriente.setEmail(DEFAULT_EMAIL_TEST);
		}

		adquiriente.setNumeroIdentificacion(facturaRTM.getNit());

		adquiriente.setTipoIdentificacion(Integer.parseInt(DEFAULT_TIPO_NIT));
		adquiriente.setPais(DEFAULT_PAIS);

		adquiriente.setTelefono(DEFAULT_MSISDN);

		// TODO Se toma el tipo persona como el tipo de identificacion dentro de la base
		// de datos por tema de aclaracion del nombre del campo en base de datos
		adquiriente.setTipoPersona(DEFAULT_REGIMEN);

		adquiriente.setNombreCompleto(facturaRTM.getRetailer());
		adquiriente.setEnvioPorEmailPlataforma(EMAIL);
		List<InvoiceAcquirerDTO> listaAdquirentes = new ArrayList<InvoiceAcquirerDTO>();
		listaAdquirentes.add(adquiriente);
		request.setListaAdquirentes(listaAdquirentes);
		request.setAplicafel(DEFAULT_APLICA_FEL);
		// Datos Pago de la factura
		InvoicePaymentDTO invoicePayment = new InvoicePaymentDTO();
		invoicePayment.setMoneda(DEFAULT_MONEDA);
		invoicePayment.setTipocompra(new Integer(DEFAULT_TIPO_COMPRA));
		invoicePayment.setCodigoMonedaCambio(DEFAULT_MONEDA);
		request.setPago(invoicePayment);

		// Armamos la lista detalle de la factura
		List<InvoiceDetailDTO> listaDetalle = new ArrayList<InvoiceDetailDTO>();
		if (null != request.getListaDetalle())
			listaDetalle = request.getListaDetalle();
		InvoiceDetailDTO invoiceDetail = new InvoiceDetailDTO();
		invoiceDetail.setCantidad(new BigDecimal(1));
		invoiceDetail.setDescripcion(DEFAULT_TIPO_PRODUCTO);
		invoiceDetail.setValorunitario(new BigDecimal(facturaRTM.getAddBalance()));
		invoiceDetail.setReferencia(facturaRTM.getRetailer());
		invoiceDetail.setTipocodigoproducto(DEFAULT_TIPO_CODIGO_PRODUCTO);
		invoiceDetail.setCodigoproducto(DEFAULT_TIPO_PRODUCTO);

		invoiceDetail.setNombreProducto(facturaRTM.getRetailer());
		invoiceDetail.setUnidadmedida(DEFAULT_UNIDAD_MEDIDAD);
		invoiceDetail.setTipoImpuesto(DEFAULT_TIPO_IMPUESTO);
		InvoiceDiscountDTO descuentoAux = new InvoiceDiscountDTO();
		descuentoAux.setPorcentajeDescuento(new BigDecimal(facturaRTM.getPercentageDiscount()));

		List<InvoiceDiscountDTO> listaDescuentosAux = new ArrayList<InvoiceDiscountDTO>();
		listaDescuentosAux.add(descuentoAux);
		invoiceDetail.setListaDescuento(listaDescuentosAux);

		InvoiceTaxDTO bfelImpuestos = new InvoiceTaxDTO();
		bfelImpuestos.setCodigoImpuestoRetencion("01");

		BigDecimal porcentTaxes = new BigDecimal(facturaRTM.getTax())
				.divide(new BigDecimal(facturaRTM.getAddBalance()).subtract(new BigDecimal(facturaRTM.getAmountDiscount())), 4, BigDecimal.ROUND_HALF_UP).multiply(new BigDecimal("100"));

		bfelImpuestos.setPorcentaje(porcentTaxes);

		bfelImpuestos.setIsAutoRetenido(DEFAULT_AUTO_RETENIDO);
		List<InvoiceTaxDTO> impuestosItem = new ArrayList<InvoiceTaxDTO>();
		impuestosItem.add(bfelImpuestos);
		invoiceDetail.setListaImpuestos(impuestosItem);

		listaDetalle.add(invoiceDetail);

		// Armamos la lista de campos adicionales
		List<InvoiceAddFieldDTO> listaCampos = new ArrayList<InvoiceAddFieldDTO>();
		InvoiceAddFieldDTO campo = new InvoiceAddFieldDTO();
		// Datos productos

		campo.setNombreCampo("observaciones");
		if (!facturaRTM.getRetailer().isEmpty()) {
			campo.setValorCampo(facturaRTM.getRetailer());
			listaCampos.add(campo);
		}
		// Datos Cliente
		campo = new InvoiceAddFieldDTO();
		campo.setNombreCampo("codigocliente");
		campo.setValorCampo(facturaRTM.getNit());
		listaCampos.add(campo);
		request.setListaCamposAdicionales(listaCampos);

		// Medios de Pago
		List<InvoicePaymentMehotdDTO> listaMediosPagos = new ArrayList<InvoicePaymentMehotdDTO>();
		InvoicePaymentMehotdDTO medioPago = new InvoicePaymentMehotdDTO();
		medioPago.setMedioPago(DEFAULT_MEDIO_PAGO);
		listaMediosPagos.add(medioPago);
		request.setListaMediosPagos(listaMediosPagos);

		request.setListaDetalle(listaDetalle);

		return request;
	}

	@Override
	public String processSalesMovilbox(Long id) throws ServiceException {
		logger.info("################init processSalesMovilbox ###########");

		TransactionBillingMovilbox sale = repositoryMovilBox.findOne(id);

		if (null == sale) {
			throw new ServiceException(new ApiError(
					generalMessages.getMessageByCod(Constants.MSG_BK_TRANSACTIONS_BILLING, "E-TRANSBILLING-03"),
					generalMessages.getMessageByCod(Constants.MSG_BK_TRANSACTIONS_BILLING, "E-TRANSBILLING-03"),
					"E-TRANSBILLING-03"));
		}

		InvoiceDocumentHeaderDTO request = createBillRequestMovilBox(sale);
		if (sale.getValor().equals("0.0") || sale.getValor().equals("0")) {
			logger.info("Facturacion de valor 0 [" + id + "] Movilbox NO factura");
			sale.setStatus("ERROR_VALUE");
			repositoryMovilBox.save(sale);
			return TransactionStatusEnum.SUCCESS.toString();
		}
		try {
			logger.info("procesando la venta [" + id + "] Movilbox request " + request.toString());
			if (!"SUCCESS".equals(sale.getStatus())) {
				Object sendTransaction = genericInvocation.executeApi(request, HttpMethod.POST, urlBillinV2,
						"/bills/createBill", billingTimeout);

				logger.info("Transaccion exitosa para la venta [" + id + "] Movilbox");
				sale.setStatus("SUCCESS");
				repositoryMovilBox.save(sale);
				return TransactionStatusEnum.SUCCESS.toString();
			}

		} catch (ServiceException se) {
			logger.error("Error de servicio de facturacion para la venta [" + id + "] Movilbox ERROR " + se.toString());

			if (sale.getTransactionsAttempts() == 0) {
				messageProductorService.sendMessageToQueue(id + "", sqsTransactionsBillingMovilboxUrlFailed);// se envia
			}
			sale.setTransactionsAttempts(sale.getTransactionsAttempts() + 1);
			sale.setStatus("FAILED");
			repositoryMovilBox.save(sale);
			logger.info("VENTA" + sale.getId() + " Encolada para reintento");

			return TransactionStatusEnum.PENDING.toString();

		} catch (Exception e) {
			logger.error("Error generico para la venta [" + id + "] Movilbox");
			throw new ServiceException(new ApiError(
					generalMessages.getMessageByCod(Constants.MSG_BK_TRANSACTIONS_BILLING, "E-TRANSBILLING-01"),
					e.getMessage(), "E-TRANSBILLING-01"));
		}

		logger.info("################ End processSalesMovilbox #################");
		return TransactionStatusEnum.PENDING.toString();
	}

	@Override
	public void initTransactionRTM() {

		logger.info("################### init initTransactionRTM ##########");
		List<TransactionBillingRTM> data = repositoryRTM.findNoProcessedRTM();
		TransactionBillingRTMProcessed billToSend = new TransactionBillingRTMProcessed();
		
		for (TransactionBillingRTM venta : data) {
			try {
				if(!venta.getType().equals(new BigInteger("2"))) {
					if (messageProductorService.sendMessageToQueue(venta.getApproval()+ "",
							sqsTransactionsBillingRTMUrl)) {
						logger.info("<" + venta.getTransaction() + "> Venta encolada para validación "
								+ venta.getTransaction());
						billToSend = mapper.convertValue(venta, TransactionBillingRTMProcessed.class);
						billToSend.setStatus("QUEUED");
						repositoryRTMProcessed.save(billToSend);
					} else
						logger.info("<" + venta.getRtmId() + "> Venta  NO encolada para validación " + venta.getRtmId());
				}
			} catch (Exception e) {
				logger.info("<" + venta.getRtmId() + "> Ocurrio un error al encolar la Venta" + venta.getRtmId());
				logger.info("<" + venta.getRtmId() + "> " + e.getMessage());
			}
		}
		logger.info("################### end initTransactionRTM ##########");

	}

	@Override
	public String processSalesRTM(String transaction) throws ServiceException {
		logger.info("################init processSalesRTM ###########");

		TransactionBillingRTMProcessed sale = repositoryRTMProcessed.findOne(transaction);
		if (null == sale) {
			throw new ServiceException(new ApiError(
					generalMessages.getMessageByCod(Constants.MSG_BK_TRANSACTIONS_BILLING, "E-TRANSBILLING-03"),
					generalMessages.getMessageByCod(Constants.MSG_BK_TRANSACTIONS_BILLING, "E-TRANSBILLING-03"),
					"E-TRANSBILLING-03"));
		}
		InvoiceDocumentHeaderDTO request = createBillRequestRTM(sale);

		try {

			if (!"SUCCESSFUL".equals(sale.getStatus())) {
				Object sendTransaction = genericInvocation.executeApi(request, HttpMethod.POST, urlBillinV2,
						"/bills/createBill", billingTimeout);
				logger.info("Transaccion exitosa para la venta [" + transaction + "] RTM");
				sale.setStatus("SUCCESSFUL");
				repositoryRTMProcessed.save(sale);
				return TransactionStatusEnum.SUCCESS.toString();
			}

		} catch (ServiceException se) {
			logger.error(
					"Error de servicio de facturacion para la venta [" + transaction + "] RTM ERROR " + se.toString());

			if (sale.getTransactionsAttempts() == 0) {
				messageProductorService.sendMessageToQueue(transaction + "", sqsTransactionsBillingRTMUrlFailed);
			}
			sale.setTransactionsAttempts(sale.getTransactionsAttempts() + 1);
			sale.setStatus("FAILED");
			repositoryRTMProcessed.save(sale);
			return TransactionStatusEnum.PENDING.toString();

		} catch (Exception e) {
			logger.error("Error generico para la venta [" + transaction + "] Movilbox");
			throw new ServiceException(new ApiError(
					generalMessages.getMessageByCod(Constants.MSG_BK_TRANSACTIONS_BILLING, "E-TRANSBILLING-01"),
					e.getMessage(), "E-TRANSBILLING-01"));
		}

		logger.info("################ End processSalesMovilbox #################");
		return TransactionStatusEnum.PENDING.toString();
	}

	@Override
	public void initTransactionRecharge() {

		logger.info("################### init initTransactionRecharge ##########");
		List<TransactionBillingRecharge> data = repositoryRecharge.findNoProcessedRecharge();
		logger.info("DATA: "+ data.toString());
		logger.info("SIZE: "+ data.size());
		
		
		for (TransactionBillingRecharge venta : data) {
			try {
				  if (messageProductorService.sendMessageToQueue(venta.getId() + "", sqsTransactionsBillingRechargeUrl)) {
					logger.info("<" + venta.getId() + "> Venta encolada para validación " + venta.getId());
					venta.setStatus("QUEUED");
					repositoryRecharge.save(venta);
				  } else
					logger.info("<" + venta.getId() + "> Venta  NO encolada para validación " + venta.getId());
			} catch (Exception e) {
				logger.info("<" + venta.getId() + "> Ocurrio un error al guardar la Venta" + venta.getId());
				logger.info("<" + venta.getId() + "> " + e.getMessage());
			}
		}
		logger.info("################### end initTransactionRTM ##########");

	}

	@Override
	public String processSalesRecharge(Long id) throws ServiceException {

		logger.info("################init processSalesMovilbox ###########");

		TransactionBillingRecharge sale = repositoryRecharge.findOne(id);
		if (null == sale) {
			throw new ServiceException(new ApiError(
					generalMessages.getMessageByCod(Constants.MSG_BK_TRANSACTIONS_BILLING, "E-TRANSBILLING-03"),
					generalMessages.getMessageByCod(Constants.MSG_BK_TRANSACTIONS_BILLING, "E-TRANSBILLING-03"),
					"E-TRANSBILLING-03"));
		}
		InvoiceDocumentHeaderDTO request = createBillRequestRecharge(sale);

		try {
			logger.info("procesando la venta [" + id + "] Recharge request " + request.toString());
			logger.info("URL billing: " + urlBillinV2);
			if (!"SUCCESSFUL".equals(sale.getStatus())) {
				Object sendTransaction = genericInvocation.executeApi(request, HttpMethod.POST, urlBillinV2,
						"/bills/createBill", billingTimeout);
				logger.info("Transaccion exitosa para la venta [" + id + "] Recharge");
				sale.setStatus("SUCCESSFUL");
				repositoryRecharge.save(sale);
				return TransactionStatusEnum.SUCCESS.toString();
			}

		} catch (ServiceException se) {
			logger.error("Error de servicio de facturacion para la venta [" + id + "] RTM ERROR " + se.toString());

			if (sale.getTransactionsAttempts() == 0) {
				messageProductorService.sendMessageToQueue(id + "", sqsTransactionsBillingRechargeUrlFailed);
			}
			sale.setTransactionsAttempts(sale.getTransactionsAttempts() + 1);
			sale.setStatus("FAILED");
			repositoryRecharge.save(sale);
			return TransactionStatusEnum.PENDING.toString();

		} catch (Exception e) {
			logger.error("Error generico para la venta [" + id + "] Movilbox");
			throw new ServiceException(new ApiError(
					generalMessages.getMessageByCod(Constants.MSG_BK_TRANSACTIONS_BILLING, "E-TRANSBILLING-01"),
					e.getMessage(), "E-TRANSBILLING-01"));
		}

		logger.info("################ End processSalesMovilbox #################");
		return TransactionStatusEnum.FAILED.toString();
	}

	private InvoiceDocumentHeaderDTO createBillRequestRecharge(TransactionBillingRecharge facturaRecharge) {

		InvoiceDocumentHeaderDTO request = new InvoiceDocumentHeaderDTO();

		CustomerResponseDTO dataClient = getSubscriberInfo(facturaRecharge.getSubscriberaddress());

		request.setVersion(DEFAULT_VERSION);
		request.setTipodocumento(DEFAULT_TIPO_DOCUMENTO);

		request.setCantidadLineas(DEFAULT_LINEAS);

		Timestamp tsFechaVenta = new Timestamp(facturaRecharge.getTickettimestamp().getTime());
		request.setFechafacturacion(tsFechaVenta);

		request.setTipoOperacion(DEFAULT_TIPO_OPERACION);

		request.setCodigoPlantillaPdf(DEFAULT_CODIGO_PLANTILLA);

		// Datos de transaccion
		ActiveTransactionDTO activeTransaction = new ActiveTransactionDTO();

		if (DEFAULT_CHANNEL_IVR.equals(facturaRecharge.getServicetype())) {
			activeTransaction.setChannel(DEFAULT_CHANNEL_IVR);
			activeTransaction.setChannel_type(DEFAULT_CHANNEL_IVR);
		} else {
			activeTransaction.setChannel(facturaRecharge.getDistributor());
			activeTransaction.setChannel_type(facturaRecharge.getDistributor());
		}

		activeTransaction.setId_transaction("RECHARGE"+facturaRecharge.getId());

		activeTransaction.setProduct_code(DEFAULT_TIPO_PROD);
		activeTransaction.setProduct_name(DEFAULT_TIPO_PROD);

		activeTransaction.setProduct_value(new BigDecimal(facturaRecharge.getTopupamount()).divide(new BigDecimal("100")).toString());
		activeTransaction.setMsisdn(facturaRecharge.getSubscriberaddress());

		request.setActiveTransaction(activeTransaction);

		// Datos adquierente
		InvoiceAcquirerDTO adquiriente = new InvoiceAcquirerDTO();
		adquiriente.setCiudad(DEFAULT_CIUDAD_CODE);
		adquiriente.setDepartamento(DEFAULT_DEPARTAMENTO_CODE);
		adquiriente.setNombreDepartamento(DEFAULT_DEPARTAMENTO);
		
		if (null != dataClient && null != dataClient.getDetails().getAddress().getLine1() && null != dataClient.getDetails().getAddress().getLine1() && !dataClient.getDetails().getAddress().getLine1().isEmpty()) {
			// Se valida para que no viajen mas de 100 caracteres
			adquiriente.setDirección(validateLength(dataClient.getDetails().getAddress().getLine1(), 100));
		}
		else {
			adquiriente.setDirección(validateLength(DEFAULT_DIRECCION, 100));
		}
		

		// Se valida para que no viajen mas de 50 caracteres
		adquiriente.setBarioLocalidad(validateLength(DEFAULT_BARRIO, 50));

		if (!devBillingEmailMode) {
			if (null != dataClient && null != dataClient.getDetails().getEmail() && !dataClient.getDetails().getEmail().isEmpty()) {
				adquiriente.setEmail(dataClient.getDetails().getEmail());
			} else {
				adquiriente.setEmail(DEFAULT_EMAIL);
			}
		} else {
			adquiriente.setEmail(DEFAULT_EMAIL_TEST);
		}
		if (null != dataClient && null != dataClient.getDetails().getDocument().getId()) {
			adquiriente.setNumeroIdentificacion(dataClient.getDetails().getDocument().getId());
		} else {
			adquiriente.setNumeroIdentificacion(DEFAULT_NIT_VIRGIN);
		}

		adquiriente.setTipoIdentificacion(Integer.parseInt(DEFAULT_CODE_CEDULA));
		adquiriente.setPais(DEFAULT_PAIS);

		adquiriente.setTelefono(facturaRecharge.getSubscriberaddress());

		// TODO Se toma el tipo persona como el tipo de identificacion dentro de la
		// base de datos por tema de aclaracion del nombre del campo en base de datos
		adquiriente.setTipoPersona(DEFAULT_REGIMEN);
		if (null != dataClient && null != dataClient.getDetails().getGivenName()) {
			if (null != dataClient.getDetails().getFamilyName()) {
				adquiriente.setNombreCompleto(dataClient.getDetails().getGivenName() + " " + dataClient.getDetails().getFamilyName());
			} else {
				adquiriente.setNombreCompleto(dataClient.getDetails().getGivenName());
			}
		} else {
			adquiriente.setNombreCompleto(facturaRecharge.getSubscriberaddress());
		}

		adquiriente.setEnvioPorEmailPlataforma(EMAIL);
		List<InvoiceAcquirerDTO> listaAdquirentes = new ArrayList<InvoiceAcquirerDTO>();
		listaAdquirentes.add(adquiriente);
		request.setListaAdquirentes(listaAdquirentes);

		// Descuentos
		InvoiceDiscountDTO descuento = new InvoiceDiscountDTO();
		descuento.setDescuento(new BigDecimal("0.0"));
		
		descuento.setPorcentajeDescuento(new BigDecimal("0.0"));
		List<InvoiceDiscountDTO> listaDescuentos = new ArrayList<InvoiceDiscountDTO>();
		listaDescuentos.add(descuento);
		request.setListaDescuentos(listaDescuentos);

		request.setAplicafel(DEFAULT_APLICA_FEL); // Datos Pago de la factura
		InvoicePaymentDTO invoicePayment = new InvoicePaymentDTO();
		invoicePayment.setMoneda(DEFAULT_MONEDA);
		invoicePayment.setTipocompra(new Integer(DEFAULT_TIPO_COMPRA));
		invoicePayment.setFechavencimiento(facturaRecharge.getExpirydate());
		invoicePayment.setCodigoMonedaCambio(DEFAULT_MONEDA);

		invoicePayment.setTotalfactura(new BigDecimal(facturaRecharge.getTopupamount()).divide(new BigDecimal("100")));
		invoicePayment.setTotalimportebruto(new BigDecimal(facturaRecharge.getTopupamount()).divide(new BigDecimal("100")));
		request.setPago(invoicePayment);

		// Armamos la lista detalle de la factura
		List<InvoiceDetailDTO> listaDetalle = new ArrayList<InvoiceDetailDTO>();

		InvoiceDetailDTO invoiceDetail = new InvoiceDetailDTO();
		invoiceDetail.setCantidad(new BigDecimal(1));
		invoiceDetail.setDescripcion(DEFAULT_TIPO_PROD); //
		invoiceDetail.setCodigoproducto(facturaRecharge.getTariffid());
		invoiceDetail.setValorunitario(new BigDecimal(facturaRecharge.getTopupamount()).divide(new BigDecimal("100")));
		invoiceDetail.setPreciototal(new BigDecimal(facturaRecharge.getTopupamount()).divide(new BigDecimal("100")));
		invoiceDetail.setPreciosinimpuestos(new BigDecimal(facturaRecharge.getTopupamount()).divide(new BigDecimal("100")));
		invoiceDetail.setReferencia(facturaRecharge.getTariffid());
		invoiceDetail.setTipocodigoproducto(DEFAULT_TIPO_CODIGO_PRODUCTO);

		invoiceDetail.setCodigoproducto(DEFAULT_TIPO_PROD);

		invoiceDetail.setNombreProducto(DEFAULT_TIPO_PROD);
		invoiceDetail.setUnidadmedida(DEFAULT_UNIDAD_MEDIDAD);

		invoiceDetail.setTipoImpuesto(DEFAULT_TIPO_IMPUESTO);

		InvoiceDiscountDTO descuentoAux = new InvoiceDiscountDTO();
		descuentoAux.setDescuento(new BigDecimal("0"));
		descuentoAux.setPorcentajeDescuento(new BigDecimal("0.0"));

		List<InvoiceDiscountDTO> listaDescuentosAux = new ArrayList<InvoiceDiscountDTO>();
		listaDescuentosAux.add(descuentoAux);
		invoiceDetail.setListaDescuento(listaDescuentosAux);

		InvoiceTaxDTO bfelImpuestos = new InvoiceTaxDTO();
		bfelImpuestos.setBaseimponible(new BigDecimal(facturaRecharge.getTopupamount()).divide(new BigDecimal("100")));
		bfelImpuestos.setCodigoImpuestoRetencion("01");
		bfelImpuestos.setPorcentaje(new BigDecimal("0.0"));
		bfelImpuestos.setValorImpuestoRetencion(new BigDecimal("0.0"));
		bfelImpuestos.setIsAutoRetenido(DEFAULT_AUTO_RETENIDO);
		List<InvoiceTaxDTO> impuestosItem = new ArrayList<InvoiceTaxDTO>();
		impuestosItem.add(bfelImpuestos);
		invoiceDetail.setListaImpuestos(impuestosItem);

		listaDetalle.add(invoiceDetail);

		// Armamos la lista de campos adicionales
		List<InvoiceAddFieldDTO> listaCampos = new ArrayList<InvoiceAddFieldDTO>();
		InvoiceAddFieldDTO campo = new InvoiceAddFieldDTO(); // Datos productos

		campo.setNombreCampo("observaciones");
		if (!facturaRecharge.getSessionid().isEmpty()) {
			campo.setValorCampo(facturaRecharge.getSessionid());
			listaCampos.add(campo);
		}
		// Datos Cliente
		campo = new InvoiceAddFieldDTO();
		campo.setNombreCampo("codigocliente");
		campo.setValorCampo(facturaRecharge.getSubscriberaddress());
		listaCampos.add(campo);
		request.setListaCamposAdicionales(listaCampos);

		// Medios de Pago
		List<InvoicePaymentMehotdDTO> listaMediosPagos = new ArrayList<InvoicePaymentMehotdDTO>();
		InvoicePaymentMehotdDTO medioPago = new InvoicePaymentMehotdDTO();
		medioPago.setMedioPago(DEFAULT_MEDIO_PAGO);
		listaMediosPagos.add(medioPago);
		request.setListaMediosPagos(listaMediosPagos);

		request.setListaDetalle(listaDetalle);

		return request;
	}

	private Town getCity(String city) throws ServiceException {
		logger.info("Inicia metodo convertCity");
		Town town = new Town();
		if (null == city || "".equals(city) || "N/A".equals(city)) {
			town.setDepartment(DEFAULT_DEPARTAMENTO_CODE);
			town.setName(DEFAULT_CIUDAD_CODE);
			town.setDaneTown(DEFAULT_CIUDAD_CODE);
			town.setDaneDept(DEFAULT_DEPARTAMENTO_CODE);
			return town;
		}
		String nameCity = StringUtils.getStringBetween(city, "", "\\(");
		Object object = genericInvocation.executeApi(null, HttpMethod.GET, endpointMipaquete,
				"/getTownByName?name=" + nameCity, timeOutMipaquete);
		if (null != object) {
			town = mapper.convertValue(object, Town.class);
			if (null == town.getDepartment() || "".equals(town.getDepartment())) {
				town.setDepartment(DEFAULT_DEPARTAMENTO_CODE);
			}
		} else {
			town.setDepartment(DEFAULT_DEPARTAMENTO_CODE);
			town.setName(DEFAULT_CIUDAD_CODE);
			town.setDaneDept(DEFAULT_DEPARTAMENTO_CODE);
			town.setDaneTown(DEFAULT_CIUDAD_CODE);
		}
		logger.info("Finaliza metodo convertCity");
		return town;
	}

	private boolean validateNumber(String numero) {
		try {
			BigInteger convert = new BigInteger(numero);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	
	private  CustomerResponseDTO getSubscriberInfo(String msisdn) {
		Object client = null;
		try {
			String tokeninew = inewSecurityAccess.getTokenInew();
			SubscriptionCriteria  dataclient = new SubscriptionCriteria();
			dataclient.setMsisdn(msisdn);
			client = genericInvocation.executeApiWithTokenInew(dataclient, HttpMethod.POST, endpointInew,
					"/customerService/search/customer/bysubscription", 10000, tokeninew);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			logger.error("Error en la invocacion ");
			return null;
		}
		CustomerResponseDTO usuario = mapper.convertValue(client, CustomerResponseDTO.class);
		return usuario;
	}

}
