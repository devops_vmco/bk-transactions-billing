package co.com.virgin.transactions.billing.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 * La clase de persistencia para la base de datos billing_transactions
 * @author crist
 *
 */
@Entity
@Table(name = "billing_transactions")
@NamedQuery(name = "TransactionBillingMovilbox.findAll", query = "SELECT bt FROM TransactionBillingMovilbox bt")
public class TransactionBillingMovilbox implements Serializable {
	
	/** Serial default */
	private static final long serialVersionUID = 1L;
	
	public  TransactionBillingMovilbox() {
		super();
	}
	
	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "id_venta")
	private String idVenta;
	
	@Column(name = "nombre_regional")
	private String nombreRegional;
	
	@Column(name = "tipo_canal")
	private String tipoCanal;
	
	@Column(name = "nombre_canal")
	private String nombreCanal;
	
	@Column(name = "codigo_bodega")
	private String codigoBodega;
	
	@Column(name = "nombre_bodega")
	private String nombreBodega;
	
	@Column(name = "supervisor")
	private String supervisor;
	
	@Column(name = "vendedor")
	private String vendedor;
	
	@Column(name = "cedula_vendedor")
	private String cedulaVendedor;
	
	@Column(name = "tipo_producto")
	private String tipoProducto;
	
	@Column(name = "referencia")
	private String referencia;
	
	@Column(name = "pn")
	private String pn;
	
	@Column(name = "fecha_venta")
	@Temporal(TemporalType.DATE)
	private Date fechaVenta;
	
	@Column(name = "serial")
	private String serial;
	
	@Column(name = "id_transaccion")
	private String idTransaccion;
	
	@Column(name = "tipo_facturacion")
	private String tipoFacturacion;
	
	@Column(name = "tipo_identificacion")
	private String tipoIdentificacion;
	
	@Column(name = "tipo_documento")
	private String tipoDocumento;
	
	@Column(name = "nombres")
	private String nombres;
	
	@Column(name = "apellidos")
	private String apellidos;
	
	@Column(name = "correo_electronico")
	private String correoElectronico;
	
	@Column(name = "telefono")
	private String telefono;
	
	@Column(name = "direccion")
	private String direccion;
	
	@Column(name = "barrio")
	private String barrio;
	
	@Column(name = "departamento")
	private String departamento;
	
	@Column(name = "ciudad")
	private String ciudad;
	
	@Column(name = "last_update")
	@Temporal(TemporalType.DATE)
	private Date lastUpdate;
	
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "valor") 
	private String valor;
	
	@Column(name = "documento") 
	private String nroDocumento;
	
	@Column(name = "transactions_attempts")
	private int transactionsAttempts;
	
	
	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public int getTransactionsAttempts() {
		return transactionsAttempts;
	}
	

	public void setTransactionsAttempts(int transactionsAttempts) {
		this.transactionsAttempts = transactionsAttempts;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}
	
	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}
	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}


	public String getIdVenta() {
		return idVenta;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}

	public void setIdVenta(String idVenta) {
		this.idVenta = idVenta;
	}

	public String getNombreRegional() {
		return nombreRegional;
	}

	public void setNombreRegional(String nombreRegional) {
		this.nombreRegional = nombreRegional;
	}

	public String getTipoCanal() {
		return tipoCanal;
	}

	public void setTipoCanal(String tipoCanal) {
		this.tipoCanal = tipoCanal;
	}

	public String getNombreCanal() {
		return nombreCanal;
	}

	public void setNombreCanal(String nombreCanal) {
		this.nombreCanal = nombreCanal;
	}

	public String getCodigoBodega() {
		return codigoBodega;
	}

	public void setCodigoBodega(String codigoBodega) {
		this.codigoBodega = codigoBodega;
	}

	public String getNombreBodega() {
		return nombreBodega;
	}

	public void setNombreBodega(String nombreBodega) {
		this.nombreBodega = nombreBodega;
	}

	public String getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}

	public String getVendedor() {
		return vendedor;
	}

	public void setVendedor(String vendedor) {
		this.vendedor = vendedor;
	}

	public String getCedulaVendedor() {
		return cedulaVendedor;
	}

	public void setCedulaVendedor(String cedulaVendedor) {
		this.cedulaVendedor = cedulaVendedor;
	}

	public String getTipoProducto() {
		return tipoProducto;
	}

	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public String getPn() {
		return pn;
	}

	public void setPn(String pn) {
		this.pn = pn;
	}

	public Date getFechaVenta() {
		return fechaVenta;
	}

	public void setFechaVenta(Date fechaVenta) {
		this.fechaVenta = fechaVenta;
	}

	public String getSerial() {
		return serial;
	}

	public void setSerial(String serial) {
		this.serial = serial;
	}

	public String getIdTransaccion() {
		return idTransaccion;
	}

	public void setIdTransaccion(String idTransaccion) {
		this.idTransaccion = idTransaccion;
	}

	public String getTipoFacturacion() {
		return tipoFacturacion;
	}

	public void setTipoFacturacion(String tipoFacturacion) {
		this.tipoFacturacion = tipoFacturacion;
	}

	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}

	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getCorreoElectronico() {
		return correoElectronico;
	}

	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	@Override
	public String toString() {
		return new com.google.gson.Gson().toJson(this);
	}

}
