package co.com.virgin.transactions.billing.client;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.com.virgin.commons.util.generic.Constants;
import co.com.virgin.commons.util.generic.GeneralMessages;
import co.com.virgin.commons.util.response.ApiError;
import co.com.virgin.commons.util.response.GeneralResponse;
import co.com.virgin.commons.util.response.ServiceException;

@Component
public class GenericInvocation {

	private final static Logger logger = LoggerFactory.getLogger(GenericInvocation.class.getName());

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	HttpServletRequest request;

	private GeneralMessages generalMessages = new GeneralMessages();
	private ObjectMapper mapper = new ObjectMapper();
	private GeneralResponse<String> responseError = new GeneralResponse<String>();

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object executeApi(Object dto, HttpMethod httpMethod, String endpoint, String path, Integer timeOut)
			throws ServiceException {
		Object _return;
		try {

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			//header para acceso a nueva api billing
			headers.add("Authorization","ac0a3316-f98f-45b8-8f4c-4c19c3e22cc3");
			//headers.add("Authorization",
			//		request.getHeader("Authorization") == null ? "" : request.getHeader("Authorization"));

			HttpEntity entityReq = null;
			if (null == dto) {
				entityReq = new HttpEntity(headers);
			} else {
				entityReq = new HttpEntity<Object>(dto, headers);
			}

			// Set time out
			restTemplate = new RestTemplate(clientHttpRequestFactory(timeOut));
			logger.info(">>>>>>>>>>>>>>>>>> " + endpoint + path);
			ResponseEntity<GeneralResponse> response = restTemplate.exchange(endpoint + path, httpMethod, entityReq,
					GeneralResponse.class);

			if (null != response.getBody()) {
				GeneralResponse<Object> generalResponse = response.getBody();
				if (null != generalResponse && generalResponse.isSuccess() && null != generalResponse.getData()) {
					_return = generalResponse.getData();
				} else
					_return = null;
			} else
				_return = null;

		} catch (HttpClientErrorException e) {

			try {
				responseError = mapper.reader().forType(new TypeReference<GeneralResponse<String>>() {
				}).readValue(e.getResponseBodyAsString());
				if (responseError.getApiError() != null)
					throw new ServiceException(responseError.getApiError());
			} catch (ServiceException ex) {
				throw ex;
			} catch (Exception e1) {
				throw new ServiceException(e1.getMessage());
			}
			throw new ServiceException(e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(new ApiError(
					generalMessages.getMessageByCod(Constants.MSG_COMMONS, "E-COMMONS-23") + " " + endpoint + path,
					e.getMessage(), "E-COMMONS-23"));
		}

		return _return;
	}
	
	@SuppressWarnings({})
	public Object getForObject(Object dto, String endpoint, String path, Integer timeOut) throws ServiceException {
		Object _return = null;
		try {
			// Set time out
			restTemplate = new RestTemplate(clientHttpRequestFactory(timeOut));
			logger.info(">>>>>>>>>>>> endpoint: " + endpoint + path);
			_return = restTemplate.getForObject(endpoint + path, Object.class);
		} catch (HttpClientErrorException e) {
			try {
				responseError = mapper.reader().forType(new TypeReference<GeneralResponse<String>>() {
				}).readValue(e.getResponseBodyAsString());
				if (responseError.getApiError() != null)
					throw new ServiceException(responseError.getApiError());
			} catch (ServiceException ex) {
				throw ex;
			} catch (Exception e1) {
				throw new ServiceException(e1.getMessage());
			}
			throw new ServiceException(e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(new ApiError(
					generalMessages.getMessageByCod(Constants.MSG_COMMONS, "E-COMMONS-23") + " " + endpoint + path,
					e.getMessage(), "E-COMMONS-23"));
		}
		return _return;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object postForObject(Object dto, String endpoint, String path, Integer timeOut) throws ServiceException {
		Object _return = null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity entityReq = null;
			if (null == dto) {
				entityReq = new HttpEntity(headers);
			} else {
				entityReq = new HttpEntity<Object>(dto, headers);
			}
			// Set time out
			if (null != timeOut) {
				restTemplate = new RestTemplate(clientHttpRequestFactory(timeOut));
			}
			logger.info(">>>>>>>>>>>> endpoint: " + endpoint + path);
			_return = restTemplate.postForObject(endpoint + path, entityReq, Object.class);
		} catch (HttpClientErrorException e) {
			try {
				responseError = mapper.reader().forType(new TypeReference<GeneralResponse<String>>() {
				}).readValue(e.getResponseBodyAsString());
				if (responseError.getApiError() != null)
					throw new ServiceException(responseError.getApiError());
			} catch (ServiceException ex) {
				throw ex;
			} catch (Exception e1) {
				throw new ServiceException(e1.getMessage());
			}
			throw new ServiceException(e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			throw new ServiceException(new ApiError(
					generalMessages.getMessageByCod(Constants.MSG_COMMONS, "E-COMMONS-23") + " " + endpoint + path,
					e.getMessage(), "E-COMMONS-23"));
		} catch (Throwable t) {
			logger.error(t.getMessage(), t);
			throw new ServiceException(new ApiError(
					generalMessages.getMessageByCod(Constants.MSG_COMMONS, "E-COMMONS-23") + " " + endpoint + path,
					t.getMessage(), "E-COMMONS-24"));
			
		}
		return _return;
	}
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Object executeApiWithTokenInew(Object dto, HttpMethod httpMethod, String endpoint, String path,
			Integer timeOut, String token) throws ServiceException {
		Object _return;
		try {

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.add("Authorization", token);

			HttpEntity entityReq = null;
			if (null == dto) {
				entityReq = new HttpEntity(headers);
			} else {
				entityReq = new HttpEntity<Object>(dto, headers);
			}

			// Set time out
			restTemplate = new RestTemplate(clientHttpRequestFactory(timeOut));
			logger.info(">>>>>>>>>>>>>>>>>> " + endpoint + path);
			ResponseEntity<GeneralResponse> response = restTemplate.exchange(endpoint + path, httpMethod, entityReq,
					GeneralResponse.class);

			if (null != response.getBody()) {
				GeneralResponse<Object> generalResponse = response.getBody();
				if (null != generalResponse && generalResponse.isSuccess() && null != generalResponse.getData()) {
					_return = generalResponse.getData();
				} else
					_return = null;
			} else
				_return = null;

		} catch (HttpClientErrorException e) {

			try {
				responseError = mapper.reader().forType(new TypeReference<GeneralResponse<String>>() {
				}).readValue(e.getResponseBodyAsString());
				if (responseError.getApiError() != null)
					throw new ServiceException(responseError.getApiError());
			} catch (ServiceException se1) {
				throw se1;
			} catch (Exception e1) {
				throw new ServiceException(e1.getMessage());
			}
			throw new ServiceException(e.getMessage());
		} catch (Exception e) {
			if (e instanceof org.apache.http.NoHttpResponseException) {
				logger.error("Error encontrado - NoHttpResponseException: ", e);
			}
			logger.error(e.getMessage(), e);
			throw new ServiceException(new ApiError(
					generalMessages.getMessageByCod(Constants.MSG_COMMONS, "E-COMMONS-23") + " " + endpoint + path,
					e.getMessage(), "E-COMMONS-23"));
		}

		return _return;
	}

	public ClientHttpRequestFactory clientHttpRequestFactory(Integer timeOut) {
		HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
		factory.setReadTimeout(timeOut);
		factory.setConnectTimeout(timeOut);
		return factory;
	}
}
