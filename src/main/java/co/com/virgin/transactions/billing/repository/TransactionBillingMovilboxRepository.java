package co.com.virgin.transactions.billing.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.virgin.transactions.billing.model.TransactionBillingMovilbox;


@Repository
public interface TransactionBillingMovilboxRepository extends JpaRepository<TransactionBillingMovilbox, Long> {
	
	
	List<TransactionBillingMovilbox> findByStatus(String status);
		
	List<TransactionBillingMovilbox> findByIdVenta(String idVenta);

}

