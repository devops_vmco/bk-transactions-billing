package co.com.virgin.transactions.billing.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import co.com.virgin.transactions.billing.model.TransactionBillingRecharge;

public interface TransactionBillingRechargeRepository extends JpaRepository<TransactionBillingRecharge, Long> {

	List<TransactionBillingRecharge> findById(BigInteger id);

	List<TransactionBillingRecharge> findByStatus(String status);

	@Query(value = "\r\n" + "SELECT * FROM inew_crce_recharge icr \r\n"
			+ "WHERE status = 'CHARGED' and sessioncreationtimestamp > '2021-01-25 21:00:00' limit 10", nativeQuery = true)
	
	public List<TransactionBillingRecharge> findNoProcessedRecharge();

}
