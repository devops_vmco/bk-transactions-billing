package co.com.virgin.transactions.billing.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "inew_rtm_sales_processed")
@NamedQuery(name = "TransactionBillingRTMProcessed.findAll", query = "SELECT bt FROM TransactionBillingRTMProcessed bt")
public class TransactionBillingRTMProcessed implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(name = "rtm_id")
	private String rtmId;

	@Column(name = "retailer")
	private String retailer;

	@Column(name = "nit")
	private String nit;

	
	@Column(name = "transaction")
	private Long transaction;

	@Column(name = "type")
	private BigInteger tipe;

	@Id
	@Column(name = "approval")
	private String approval;

	@Column(name = "sale_date")
	private Date sale_date;

	@Column(name = "amount")
	private BigInteger amount;

	@Column(name = "percentage_discount")
	private BigInteger percentageDiscount;

	@Column(name = "amount_discount")
	private BigInteger amountDiscount;

	@Column(name = "tax")
	private BigInteger tax;

	@Column(name = "add_balance")
	private BigInteger addBalance;

	@Column(name = "cut")
	private String cut;

	@Column(name = "prefix")
	private BigInteger prefix;

	@Column(name = "tipo")
	private BigInteger tipo;

	@Column(name = "status")
	private String status;

	@Column(name = "transactions_attempts")
	private int transactionsAttempts;

	public int getTransactionsAttempts() {
		return transactionsAttempts;
	}

	public void setTransactionsAttempts(int transactionsAttempts) {
		this.transactionsAttempts = transactionsAttempts;
	}

	public TransactionBillingRTMProcessed() {
		// TODO Auto-generated constructor stub
	}

	public String getRtmId() {
		return rtmId;
	}

	public void setRtmId(String rtmId) {
		this.rtmId = rtmId;
	}

	public String getRetailer() {
		return retailer;
	}

	public void setRetailer(String retailer) {
		this.retailer = retailer;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public Long getTransaction() {
		return transaction;
	}

	public void setTransaction(Long transaction) {
		this.transaction = transaction;
	}

	public BigInteger getTipe() {
		return tipe;
	}

	public void setTipe(BigInteger tipe) {
		this.tipe = tipe;
	}

	public String getApproval() {
		return approval;
	}

	public void setApproval(String approval) {
		this.approval = approval;
	}

	public Date getSale_date() {
		return sale_date;
	}

	public void setSale_date(Date sale_date) {
		this.sale_date = sale_date;
	}

	public BigInteger getAmount() {
		return amount;
	}

	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}

	public BigInteger getPercentageDiscount() {
		return percentageDiscount;
	}

	public void setPercentageDiscount(BigInteger percentageDiscount) {
		this.percentageDiscount = percentageDiscount;
	}

	public BigInteger getAmountDiscount() {
		return amountDiscount;
	}

	public void setAmountDiscount(BigInteger amountDiscount) {
		this.amountDiscount = amountDiscount;
	}

	public BigInteger getTax() {
		return tax;
	}

	public void setTax(BigInteger tax) {
		this.tax = tax;
	}

	public BigInteger getAddBalance() {
		return addBalance;
	}

	public void setAddBalance(BigInteger addBalance) {
		this.addBalance = addBalance;
	}

	public String getCut() {
		return cut;
	}

	public void setCut(String cut) {
		this.cut = cut;
	}

	public BigInteger getPrefix() {
		return prefix;
	}

	public void setPrefix(BigInteger prefix) {
		this.prefix = prefix;
	}

	public BigInteger getTipo() {
		return tipo;
	}

	public void setTipo(BigInteger tipo) {
		this.tipo = tipo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return new com.google.gson.Gson().toJson(this);

	}

}