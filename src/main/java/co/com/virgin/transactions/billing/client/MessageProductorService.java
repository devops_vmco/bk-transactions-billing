package co.com.virgin.transactions.billing.client;

import co.com.virgin.commons.util.response.ServiceException;

public interface MessageProductorService {

	/**
	 * Función que permite encolar un identificador de transacción en una cola en
	 * Amazon para su posterior utilización
	 * 
	 * @param transactionId
	 * @param queueUrl
	 * @return
	 * @throws ServiceException
	 */
	Boolean sendMessageToQueue(String transactionId, String queueUrl) throws ServiceException;

}
