package co.com.virgin.transactions.billing.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import co.com.virgin.transactions.billing.model.TransactionBillingRetailerInfo;


@Repository 
public interface TransactionBillingRetailerInfoRepository extends JpaRepository<TransactionBillingRetailerInfo,String> {

}
