package co.com.virgin.transactions.billing.controller;

import java.math.BigInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.virgin.commons.util.generic.Constants;
import co.com.virgin.commons.util.generic.GeneralMessages;
import co.com.virgin.commons.util.generic.MessagesConstants;
import co.com.virgin.commons.util.response.GeneralResponse;
import co.com.virgin.commons.util.response.ServiceException;
import co.com.virgin.transactions.billing.service.TransactionBillingService;
import co.com.virgin.transactions.dto.TransactionStatusEnum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/")
@Api(value = "api", description = "Operaciones relacionadas a la funcionalidad para los servicios tipo Rest expuestos para .")
public class TransactionBillingController {

	private static final Logger logger = LogManager.getLogger("transactionsBilling");

	/** Var to extract messages of bussines */
	private GeneralMessages generalMessages = new GeneralMessages();

	@Autowired
	private TransactionBillingService transactionBillingService;

	@ApiOperation("Esta operación describe el flujo de transacciones")
	@PostMapping(value = "/processTransactionsMovilbox/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<GeneralResponse<Object>> processTransactionsMovilbox(@RequestParam Long id) {
		logger.info(" Init getInfoBillingTran from bk transaction billing");
		HttpStatus status = HttpStatus.OK;
		GeneralResponse<Object> response = new GeneralResponse<Object>();
		try {
			logger.info(
					"############################### Inicio - processTransactionsMovilbox ###############################");
			response.setData(transactionBillingService.processSalesMovilbox(id));
			response.setSuccess(true);
			logger.info(
					"############################### Fin - processTransactionsMovilbox ###############################");
		} catch (ServiceException se) {
			logger.error("Error de servicio para el idVenta [" + id + "] con el error" + se);
			response.setData(TransactionStatusEnum.PENDING.toString());
			response.setSuccess(false);
			response.setApiError(se.getApiError());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (Exception e) {
			response.setData(TransactionStatusEnum.PENDING.toString());
			logger.error("Excepcion generica para el idVenta[" + id + "] con el error" + e.getMessage(), e);			
			response.setSuccess(false);
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity<GeneralResponse<Object>>(response, status);
	}

	@ApiOperation("Esta operación valida si existen registros nuevos dentro de la base de datos con la etiqueta Charged y los envia a la cola")
	@PostMapping(value = "/initTransactionMovilbox/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<GeneralResponse<Object>> initTransactionMovilbox() {
		logger.info(" Init getInfoBillingTran from bk transaction billing");
		HttpStatus status = HttpStatus.OK;
		GeneralResponse<Object> response = new GeneralResponse<Object>();
		try {
			logger.info(
					"############################### Inicio - initTransactionMovilbox ###############################");
			transactionBillingService.initTransactionMovilbox();
			response.setSuccess(true);
			logger.info(
					"############################### Fin - initTransactionMovilbox ###############################");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setSuccess(false);
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity<GeneralResponse<Object>>(response, status);
	}

	@ApiOperation("Esta operación describe el flujo de transacciones asociada al metodo de pago RTM")
	@PostMapping(value = "/processTransactionsRTM", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<GeneralResponse<Object>> processTransactionsRTM(@RequestParam String transaction) {
		logger.info("############################### Inicio - processTransactionsRTM ###############################");
		HttpStatus status = HttpStatus.OK;
		GeneralResponse<Object> response = new GeneralResponse<Object>();
		try {
			response.setData(transactionBillingService.processSalesRTM(transaction));
			response.setSuccess(true);

		} catch (ServiceException se) {
			logger.error("Error de servicio para el idVenta [" + transaction + "] con el error" + se);
			response.setData(TransactionStatusEnum.PENDING.toString());
			response.setSuccess(false);
			response.setApiError(se.getApiError());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (Exception e) {
			response.setData(TransactionStatusEnum.PENDING.toString());
			logger.error("Excepcion generica para el idVenta[" + transaction + "] con el error" + e.getMessage(), e);
			response.setSuccess(false);
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		logger.info("############################### Fin - processTransactionsRTM ###############################");
		return new ResponseEntity<GeneralResponse<Object>>(response, status);
	}

	@ApiOperation("Esta operación valida si existen registros nuevos dentro de la base de datos con la etiqueta Charged y los envia a la cola")
	@PostMapping(value = "/initTransactionRTM/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<GeneralResponse<Object>> initTransactionRTM() {
		logger.info("############################### Inicio - initTransactionRTM ###############################");
		HttpStatus status = HttpStatus.OK;
		GeneralResponse<Object> response = new GeneralResponse<Object>();
		try {
			transactionBillingService.initTransactionRTM();
			response.setSuccess(true);
			logger.info(
					"############################### Fin - initTransactionRTM ###############################");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setSuccess(false);
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		return new ResponseEntity<GeneralResponse<Object>>(response, status);
	}

	@ApiOperation("Esta operación describe el flujo de transacciones asociada a los metodos de pago ATH, IVR y Davivienda")
	@PostMapping(value = "/processTransactionsRecharge", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<GeneralResponse<Object>> processTransactionsRecharge(@RequestParam Long id) {
		
		HttpStatus status = HttpStatus.OK;
		GeneralResponse<Object> response = new GeneralResponse<Object>();
		try {
			logger.info(
					"############################### Inicio - processTransactionsRecharge ###############################");
			response.setData(transactionBillingService.processSalesRecharge(id));
			response.setSuccess(true);

		} catch (ServiceException se) {
			logger.error("Error de servicio para el idVenta [" + id + "] con el error" + se);
			response.setData(TransactionStatusEnum.PENDING.toString());
			response.setSuccess(false);
			response.setApiError(se.getApiError());
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		} catch (Exception e) {
			response.setData(TransactionStatusEnum.PENDING.toString());
			logger.error("Excepcion generica para el idVenta[" + id + "] con el error" + e.getMessage(), e);
			response.setSuccess(false);
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		logger.info(
				"############################### Fin - processTransactionsRecharge ###############################");
		return new ResponseEntity<GeneralResponse<Object>>(response, status);
	}

	@ApiOperation("Esta operación valida si existen registros nuevos dentro de la base de datos inew recharge con la etiqueta Charged y los envia a la cola")
	@PostMapping(value = "/initTransactionRecharge/", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public ResponseEntity<GeneralResponse<Object>> initTransactionRecharge() {
		logger.info(" Init initTransactionRecharge from bk transaction billing");
		HttpStatus status = HttpStatus.OK;
		GeneralResponse<Object> response = new GeneralResponse<Object>();
		try {
			logger.info(
					"############################### Inicio - initTransactionRecharge ###############################");
			transactionBillingService.initTransactionRecharge();
			response.setSuccess(true);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setSuccess(false);
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		logger.info("############################### Fin - initTransactionRecharge ###############################");
		return new ResponseEntity<GeneralResponse<Object>>(response, status);
	}

	@ApiOperation(value = "EndPoint test para comprobar funcionalidad bk-transactions-billing.", response = ResponseEntity.class)
	@GetMapping("/test")
	public ResponseEntity<GeneralResponse<String>> getTest() {
		logger.info(" Init getTest from bk subscriber information");
		HttpStatus status = HttpStatus.OK;
		GeneralResponse<String> response = new GeneralResponse<String>();
		try {
			response.setData("Hellow Virgin Mobile from bk-transactions-billing");
			response.setSuccess(true);
			response.setMessage(
					generalMessages.getMessageByCod(Constants.MSG_COMMONS, MessagesConstants.MESSAGE_SUCCESS));
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			response.setSuccess(false);
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}
		logger.info("End getTest from bk transactions billing");
		return new ResponseEntity<GeneralResponse<String>>(response, status);
	}

}
