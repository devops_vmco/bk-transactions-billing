package co.com.virgin.transactions.billing.repository;

import java.math.BigInteger;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import co.com.virgin.transactions.billing.model.TransactionBillingRTM;




public interface TransactionBillingRTMRepository extends JpaRepository<TransactionBillingRTM,Long>{
	
	@Query(value = "SELECT *\r\n"
	+ "  FROM inew_rtm_sales irs \r\n"
	+ " WHERE irs.\"transaction\" NOT IN (SELECT \"transaction\" \r\n"
	+ "                       FROM inew_rtm_sales_processed irsp) and status = 'CHARGED' limit 300", nativeQuery = true)

	List<TransactionBillingRTM> findNoProcessedRTM();
	
	List<TransactionBillingRTM> findByStatus(String status);
	
	List<TransactionBillingRTM> findByRtmId(String rtmId);
	
	List<TransactionBillingRTM> findByTransaction(BigInteger transaction);
	
}
