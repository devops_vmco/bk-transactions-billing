package co.com.virgin.transactions.billing.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "inew_crce_recharge")
public class TransactionBillingRecharge implements Serializable{
	
	/** Serial default */
	private static final long serialVersionUID = 1L; 
	
	@Id
	@Column(name = "id")
    private Long id;
	
	@Column(name = "headerversion")
    private String headerversion;
	
	@Column(name = "tickettypeid")
    private Long tickettypeid;
	
	@Column(name = "success")
    private boolean success;
	
	@Column(name = "provider")
    private String provider;
	
	@Column(name = "application")
    private String application;
	
	@Column(name = "tickettype")
    private String tickettype;
	
	@Column(name = "nodeid")
    private String nodeid; 
	
	@Column(name="tickettimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date tickettimestamp;
	
	@Column(name="sessioncreationtimestamp")
	@Temporal(TemporalType.TIMESTAMP)
	private Date sessioncreationtimestamp;
	
	@Column(name = "sessionid")
    private String sessionid;
    
	@Column(name = "transactionid")
    private String transactionid;
	
	@Column(name = "sequenceid")
    private String sequenceid;
    
	@Column(name = "subscriberid")
    private Long subscriberid;
	
	@Column(name = "subscriberaddress")
    private String subscriberaddress;
    
	@Column(name = "subscribertype")
    private String subscribertype;
    
	@Column(name = "errorcode")
    private String errorcode;
	
	@Column(name = "groupid")
    private Long groupid;
    
	@Column(name = "subscriberstate")
    private String subscriberstate;
	
	@Column(name = "subscriberstateinfo")
    private String subscriberstateinfo;
	
	@Column(name = "languageid")
    private Long languageid;
	
	@Column(name = "chargenotification")
    private String chargenotification;
    
	@Column(name = "recordversion")
    private String recordversion;
	
	@Column(name = "crceoperation")
    private String crceoperation;
	
	@Column(name = "activefeature")
    private String activefeature;
	
	@Column(name = "bundlecode")
    private String bundlecode;
	
	@Column(name = "campaignid")
    private Long campaignid;
	
	@Column(name = "resetaction")
    private String resetaction;
	
	@Column(name = "servicetype")
    private String servicetype;
   
	@Column(name = "customercareuser")
    private String customercareuser;
	
	@Column(name = "distributor")
    private String distributor;
    
	@Column(name = "subdistributor")
    private String subdistributor;
	
	@Column(name = "posid")
    private String posid;
	
	@Column(name = "voucherid")
    private String voucherid;
    
	@Column(name = "voucherseries")
    private String voucherseries;
	
	@Column(name = "paymentmethod")
	private String paymentmethod;
	
	@Column(name = "tariffid")
	private String tariffid;
	
	@Column(name = "accountid")
    private Long accountid;
	
	@Column(name = "accountdifference")
    private Long accountdifference;
	
	@Column(name = "topupamount")
    private Long topupamount;
    
	@Column(name = "currency")
    private String currency;
	
	@Column(name = "closingbalance")
    private Long closingbalance;
	
	@Column(name = "accountexpirydate")
	@Temporal(TemporalType.DATE)
    private Date accountexpirydate;
	
	@Column(name = "expirydate")
	@Temporal(TemporalType.DATE)
    private Date expirydate;
	
	@Column(name = "crceresultcode")
    private String crceresultcode;
	
	@Column(name = "transparentdata")
    private String transparentdata;
	
	@Column(name = "invoiceid")
    private Long invoiceid;
    
	@Column(name = "prefix")
    private String prefix;
	
	@Column(name = "tipo")
    private String tipo;

	@Column(name = "transactions_attempts")
	private Long transactionsAttempts;
	
	@Column(name = "status")
	private String status;

	
	
	
	public String getTariffid() {
		return tariffid;
	}

	public void setTariffid(String tariffid) {
		this.tariffid = tariffid;
	}

	public Date getTickettimestamp() {
		return tickettimestamp;
	}

	public void setTickettimestamp(Date tickettimestamp) {
		this.tickettimestamp = tickettimestamp;
	}

	public Date getSessioncreationtimestamp() {
		return sessioncreationtimestamp;
	}

	public void setSessioncreationtimestamp(Date sessioncreationtimestamp) {
		this.sessioncreationtimestamp = sessioncreationtimestamp;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getHeaderversion() {
		return headerversion;
	}

	public void setHeaderversion(String headerversion) {
		this.headerversion = headerversion;
	}

	public Long getTickettypeid() {
		return tickettypeid;
	}

	public void setTickettypeid(Long tickettypeid) {
		this.tickettypeid = tickettypeid;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public String getTickettype() {
		return tickettype;
	}

	public void setTickettype(String tickettype) {
		this.tickettype = tickettype;
	}

	public String getNodeid() {
		return nodeid;
	}

	public void setNodeid(String nodeid) {
		this.nodeid = nodeid;
	}

	public String getSessionid() {
		return sessionid;
	}

	public void setSessionid(String sessionid) {
		this.sessionid = sessionid;
	}

	public String getTransactionid() {
		return transactionid;
	}

	public void setTransactionid(String transactionid) {
		this.transactionid = transactionid;
	}

	public String getSequenceid() {
		return sequenceid;
	}

	public void setSequenceid(String sequenceid) {
		this.sequenceid = sequenceid;
	}

	public Long getSubscriberid() {
		return subscriberid;
	}

	public void setSubscriberid(Long subscriberid) {
		this.subscriberid = subscriberid;
	}

	public String getSubscriberaddress() {
		return subscriberaddress;
	}

	public void setSubscriberaddress(String subscriberaddress) {
		this.subscriberaddress = subscriberaddress;
	}

	public String getSubscribertype() {
		return subscribertype;
	}

	public void setSubscribertype(String subscribertype) {
		this.subscribertype = subscribertype;
	}

	public String getErrorcode() {
		return errorcode;
	}

	public void setErrorcode(String errorcode) {
		this.errorcode = errorcode;
	}

	public Long getGroupid() {
		return groupid;
	}

	public void setGroupid(Long groupid) {
		this.groupid = groupid;
	}

	public String getSubscriberstate() {
		return subscriberstate;
	}

	public void setSubscriberstate(String subscriberstate) {
		this.subscriberstate = subscriberstate;
	}

	public String getSubscriberstateinfo() {
		return subscriberstateinfo;
	}

	public void setSubscriberstateinfo(String subscriberstateinfo) {
		this.subscriberstateinfo = subscriberstateinfo;
	}

	public Long getLanguageid() {
		return languageid;
	}

	public void setLanguageid(Long languageid) {
		this.languageid = languageid;
	}

	public String getChargenotification() {
		return chargenotification;
	}

	public void setChargenotification(String chargenotification) {
		this.chargenotification = chargenotification;
	}

	public String getRecordversion() {
		return recordversion;
	}

	public void setRecordversion(String recordversion) {
		this.recordversion = recordversion;
	}

	public String getCrceoperation() {
		return crceoperation;
	}

	public void setCrceoperation(String crceoperation) {
		this.crceoperation = crceoperation;
	}

	public String getActivefeature() {
		return activefeature;
	}

	public void setActivefeature(String activefeature) {
		this.activefeature = activefeature;
	}

	public String getBundlecode() {
		return bundlecode;
	}

	public void setBundlecode(String bundlecode) {
		this.bundlecode = bundlecode;
	}

	public Long getCampaignid() {
		return campaignid;
	}

	public void setCampaignid(Long campaignid) {
		this.campaignid = campaignid;
	}

	public String getResetaction() {
		return resetaction;
	}

	public void setResetaction(String resetaction) {
		this.resetaction = resetaction;
	}

	public String getServicetype() {
		return servicetype;
	}

	public void setServicetype(String servicetype) {
		this.servicetype = servicetype;
	}

	public String getCustomercareuser() {
		return customercareuser;
	}

	public void setCustomercareuser(String customercareuser) {
		this.customercareuser = customercareuser;
	}

	public String getDistributor() {
		return distributor;
	}

	public void setDistributor(String distributor) {
		this.distributor = distributor;
	}

	public String getSubdistributor() {
		return subdistributor;
	}

	public void setSubdistributor(String subdistributor) {
		this.subdistributor = subdistributor;
	}

	public String getPosid() {
		return posid;
	}

	public void setPosid(String posid) {
		this.posid = posid;
	}

	public String getVoucherid() {
		return voucherid;
	}

	public void setVoucherid(String voucherid) {
		this.voucherid = voucherid;
	}

	public String getVoucherseries() {
		return voucherseries;
	}

	public void setVoucherseries(String voucherseries) {
		this.voucherseries = voucherseries;
	}

	public String getPaymentmethod() {
		return paymentmethod;
	}

	public void setPaymentmethod(String paymentmethod) {
		this.paymentmethod = paymentmethod;
	}

	public Long getAccountid() {
		return accountid;
	}

	public void setAccountid(Long accountid) {
		this.accountid = accountid;
	}

	public Long getAccountdifference() {
		return accountdifference;
	}

	public void setAccountdifference(Long accountdifference) {
		this.accountdifference = accountdifference;
	}

	public Long getTopupamount() {
		return topupamount;
	}

	public void setTopupamount(Long topupamount) {
		this.topupamount = topupamount;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Long getClosingbalance() {
		return closingbalance;
	}

	public void setClosingbalance(Long closingbalance) {
		this.closingbalance = closingbalance;
	}

	public Date getAccountexpirydate() {
		return accountexpirydate;
	}

	public void setAccountexpirydate(Date accountexpirydate) {
		this.accountexpirydate = accountexpirydate;
	}

	public Date getExpirydate() {
		return expirydate;
	}

	public void setExpirydate(Date expirydate) {
		this.expirydate = expirydate;
	}

	public String getCrceresultcode() {
		return crceresultcode;
	}

	public void setCrceresultcode(String crceresultcode) {
		this.crceresultcode = crceresultcode;
	}

	public String getTransparentdata() {
		return transparentdata;
	}

	public void setTransparentdata(String transparentdata) {
		this.transparentdata = transparentdata;
	}

	public Long getInvoiceid() {
		return invoiceid;
	}

	public void setInvoiceid(Long invoiceid) {
		this.invoiceid = invoiceid;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Long getTransactionsAttempts() {
		return transactionsAttempts;
	}

	public void setTransactionsAttempts(Long transactionsAttempts) {
		this.transactionsAttempts = transactionsAttempts;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	@Override
	public String toString() {
		return new com.google.gson.Gson().toJson(this);
	}
	
	
}
